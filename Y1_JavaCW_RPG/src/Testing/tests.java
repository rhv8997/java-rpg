package Testing;
import static org.junit.Assert.*;

import Game.Characters.*;
import Game.Items.*;
import Game.Rooms.Forest;
import Game.Rooms.Rooms;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by c1639984 on 22/04/2017.
 */
public class tests {
    private static ArrayList<Monster> enemies = new ArrayList<Monster>();
    private static ArrayList<Weapons> weaponInventory = new ArrayList<Weapons>();
    private static ArrayList<Healing> healingInventory = new ArrayList<Healing>();
    private static ArrayList<Rooms> RoomsAvailable = new ArrayList<Rooms>();


    public tests() {
        getEnemies().add(new Dragon());
        getEnemies().add(new Gnome());
        getEnemies().add(new Siren());
        getEnemies().add(new Spider());
        getWeaponInventory().add(new Sword());
        getWeaponInventory().add(new Wand());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
    };

    public static ArrayList<Monster> getEnemies() {
        return enemies;
    }

    public static void setEnemies(ArrayList<Monster> enemies) {
        tests.enemies = enemies;
    }

    public static ArrayList<Weapons> getWeaponInventory() {
        return weaponInventory;
    }

    public static void setWeaponInventory(ArrayList<Weapons> weaponInventory) {
        tests.weaponInventory = weaponInventory;
    }

    public static ArrayList<Healing> getHealingInventory() {
        return healingInventory;
    }

    public static void setHealingInventory(ArrayList<Healing> healingInventory) {
        tests.healingInventory = healingInventory;
    }

    public static ArrayList<Rooms> getRoomsAvailable() {
        return RoomsAvailable;
    }

    public static void setRoomsAvailable(ArrayList<Rooms> roomsAvailable) {
        RoomsAvailable = roomsAvailable;
    }

    @Test
    public void monsterIsADragon(){
        Monster Enemy = getEnemies().get(0);
        assertEquals("DRAGON", Enemy.getMonsterName());
    }
    @Test
    public void monsterIsAGnome(){
        Monster Enemy = getEnemies().get(1);
        assertEquals("GNOME", Enemy.getMonsterName());
    }
    @Test
    public void monsterIsASiren(){
        Monster Enemy = getEnemies().get(2);
        assertEquals("SIREN", Enemy.getMonsterName());
    }
    @Test
    public void monsterIsASpider(){
        Monster Enemy = getEnemies().get(3);
        assertEquals("SPIDER", Enemy.getMonsterName());
    }

    @Test
    public void initialPlayerStatusCondition(){
        Player currentPlayer = new Player();
        assertEquals("NONE",currentPlayer.getCurrentStatusCondition().toString());
    }
    @Test
    public void initialPlayerHealthPoints(){
        Player currentPlayer = new Player();
        assertEquals("100",currentPlayer.getMaxHealthPoints().toString());
    }
    @Test
    public void initialPlayerEnergyLevels(){
        Player currentPlayer = new Player();
        assertEquals("50",currentPlayer.getMaxEnergyLevel().toString());
    }

    @Test
    public void playerIsFrozen(){
        Player currentPlayer = new Player();
        Monster Enemy = getEnemies().get(2);
        currentPlayer.setCurrentStatusCondition(Enemy.getInflictStatusCondition());
        assertEquals("FROZEN",currentPlayer.getCurrentStatusCondition().toString());
    }
    @Test
    public void playerIsBurned(){
        Player currentPlayer = new Player();
        Monster Enemy = getEnemies().get(0);
        currentPlayer.setCurrentStatusCondition(Enemy.getInflictStatusCondition());
        assertEquals("BURNED",currentPlayer.getCurrentStatusCondition().toString());
    }
    @Test
    public void playerIsPoisoned(){
        Player currentPlayer = new Player();
        Monster Enemy = getEnemies().get(3);
        currentPlayer.setCurrentStatusCondition(Enemy.getInflictStatusCondition());
        assertEquals("POISONED",currentPlayer.getCurrentStatusCondition().toString());
    }

    @Test
    public void burnDecreasesHealthBy10hp(){
        Player currentPlayer = new Player();
//        THE MONSTER IS A DRAGON
        Monster Enemy = getEnemies().get(0);
        currentPlayer.setCurrentStatusCondition(Enemy.getInflictStatusCondition());
        if (currentPlayer.getCurrentStatusCondition().equals(statusCondition.BURNED)) {
            currentPlayer.setMaxHealthPoints(currentPlayer.getMaxHealthPoints() - 10);
        }
        assertEquals("90",currentPlayer.getMaxHealthPoints().toString());
    }


    @Test
    public void posionDecreaseshEnergyBy10(){
        Player currentPlayer = new Player();
        Monster Enemy = getEnemies().get(3);
        currentPlayer.setCurrentStatusCondition(Enemy.getInflictStatusCondition());
        if (currentPlayer.getCurrentStatusCondition().equals(statusCondition.POISONED)) {
            currentPlayer.setMaxEnergyLevel(currentPlayer.getMaxEnergyLevel() - 10);
        }
        assertEquals("40",currentPlayer.getMaxEnergyLevel().toString());
    }
    @Test
    public void initialWeaponsInventoryContainsTwoItems(){
//        WHEN RUNNING ALL TESTS, THE ITEMS ARE ADDED TO THE LIST MANY TIMES SO THERE WERE MORE THAN 2 ITEMS IN THE LIST
//        SO I CLEARED THE LIST AND ADDED THE ITEMS AGAIN
        getWeaponInventory().clear();
        getWeaponInventory().add(new Sword());
        getWeaponInventory().add(new Wand());

        assertEquals(2, getWeaponInventory().size());
    }

    @Test
    public void initialWeaponsInventoryContainsSword(){
        Weapons weapon = getWeaponInventory().get(0);
        assertEquals("SWORD",weapon.getItemName().toString());
    }
    @Test
    public void initialWeaponsInventoryContainsWand(){
        Weapons weapon = getWeaponInventory().get(1);
        assertEquals("WAND",weapon.getItemName().toString());
    }

    @Test
    public void initialHealingInventoryContainsSixItems(){

//        WHEN RUNNING ALL TESTS, THE ITEMS ARE ADDED TO THE LIST MANY TIMES SO THERE WERE MORE THAN 6 ITEMS IN THE LIST
//        SO I CLEARED THE LIST AND ADDED THE ITEMS AGAIN
        getHealingInventory().clear();
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());

        assertEquals(6, getHealingInventory().size());
    }

    @Test
    public void initialHealingInventoryContainsPotion(){
        Healing healItem = getHealingInventory().get(0);
        assertEquals("POTION",healItem.getItemName().toString());
    }
    @Test
    public void initialHealingInventoryContainsMushroom(){
        Healing healItem = getHealingInventory().get(3);
        assertEquals("MUSHROOM",healItem.getItemName().toString());
    }

    @Test
    public void addItemToAList(){
        getWeaponInventory().add(new goldenKey());
        assertEquals("GOLDEN KEY", getWeaponInventory().get(getWeaponInventory().size()-1).getItemName().toString());
    }

    @Test
    public void removeAnItemFromAList(){
        //        WHEN RUNNING ALL TESTS, THE ITEMS ARE ADDED TO THE LIST MANY TIMES SO THERE WERE MORE THAN 6 ITEMS IN THE LIST
//        SO I CLEARED THE LIST AND ADDED THE ITEMS AGAIN
        getHealingInventory().clear();
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
        getHealingInventory().remove(getHealingInventory().size()-1);

        assertEquals(5, getHealingInventory().size());
    }

    @Test
    public void moveToNewRoom(){
        Rooms currentRoom = new Forest();
        assertEquals("LAKE",currentRoom.getNextRoom().getRoomName());
    }

    @Test
    public void goldenKeyIsInInventory(){
        getWeaponInventory().add(new goldenKey());
        String isInInventory = "FALSE";
        for (int i = 0; i < getWeaponInventory().size(); i++) {
            if ((getWeaponInventory().get(i).getItemName().equals("GOLDEN KEY"))) {
                isInInventory = "TRUE";
            }
            }
        assertEquals("TRUE",isInInventory.toString());

    }
    @Test
    public void goldenKeyIsNotInInventory(){
        String isInInventory = "FALSE";
        for (int i = 0; i < getWeaponInventory().size(); i++) {
            if ((getWeaponInventory().get(i).getItemName().equals("GOLDEN KEY"))) {
                isInInventory = "TRUE";
            }
        }
        assertEquals("FALSE",isInInventory.toString());

    }

    @Test
    public void weaponDecreasesMonsterHealth(){
        Monster dragon = getEnemies().get(0);
        Weapons sword = getWeaponInventory().get(0);
        dragon.setMonsterHealth(dragon.getMonsterHealth()-sword.getHitPoints());
        assertEquals("70",dragon.getMonsterHealth().toString());

    }





}
