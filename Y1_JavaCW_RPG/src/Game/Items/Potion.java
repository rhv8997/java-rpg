package Game.Items;

/**
 * Created by c1639984 on 28/02/2017.
 */
public class Potion extends Healing {
    public Potion(String itemName, String itemDescription, Integer healPoints, itemType itemTypes) {
        super(itemName, itemDescription, healPoints, itemTypes);
    }

    public Potion() {
    }

    @Override
    public String getItemName() {
        return "POTION";
    }

    @Override
    public String getItemDescription() {
        return  "A drink that will restore health to 100%";
    }

    @Override
    public Integer getHealPoints() {
        return 100;
    }

    @Override
    public itemType getItemTypes() {
        return itemType.HEALING;
    }
}
