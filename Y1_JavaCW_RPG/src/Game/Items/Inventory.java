package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Inventory {
    private String itemName;
    private String itemDescription;
    private itemType itemTypes;

    public Inventory(String itemName, String itemDescription, itemType itemTypes) {
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemTypes = itemTypes;
    }

    public Inventory() {
    }

    public String getItemName() {
        return itemName;
    }

//    ITEM COULD BE A HEALING ITEM OR A WEAPON. USED WHEN COLLECTING TIEMS TO GO INTO THE RIGHT INVENTORY
    public itemType getItemTypes(){return itemTypes;}

    public String getItemDescription() {
        return itemDescription;
    }
}
