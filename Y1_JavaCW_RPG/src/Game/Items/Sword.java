package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Sword extends Weapons {

    public Sword(String itemName, String itemDescription,Integer hitPoints,itemType itemTypes) {
        super(itemName, itemDescription, hitPoints, itemTypes);
    }

    public Sword() {
    }

    @Override
    public String getItemName() {
        return "SWORD";
    }

    @Override
    public String getItemDescription() {
        return "A sword that can inflict a lot of damage by piercing the enemies body";
    }

    @Override
    public Integer getHitPoints() {
        return 30;
    }

    @Override
    public itemType getItemTypes() {
        return itemType.WEAPON;
    }
}
