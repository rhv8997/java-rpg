package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class goldenKey extends Weapons {


    public goldenKey() {
    }


    @Override
    public String getItemName() {
        return "GOLDEN KEY";
    }

    @Override
    public String getItemDescription() {
        return"A key that looks like it can open a treasure chest";
    }
    public itemType getItemTypes() {
        return itemType.WEAPON;
    }

}
