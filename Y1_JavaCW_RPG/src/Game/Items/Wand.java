package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Wand extends Weapons {

    public Wand(String itemName, String itemDescription, Integer hitPoints, itemType itemTypes) {
        super(itemName, itemDescription,hitPoints, itemTypes);
    }

    public Wand() {
    }
    @Override
    public String getItemName() {
        return "WAND";
    }

    @Override
    public String getItemDescription() {
        return "Cast a powerful spell upon the enemy which can reduce the enemies health points";
    }

    @Override
    public Integer getHitPoints() {
        return 20;
    }

    @Override
    public itemType getItemTypes() {
        return itemType.WEAPON;
    }
}
