package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Healing extends Inventory {
//    HOW MUCH YOU WILL BE HEALED BY
    private Integer healPoints;

    public Healing(String itemName, String itemDescription, Integer healPoints, itemType itemTypes) {
        super(itemName, itemDescription, itemTypes);
        this.healPoints = healPoints;
    }
//EMPTY CONSTRUCTOR FOR A HEALING ITEM WITH SET STATS

    public Healing() {
    }


    @Override
    public String getItemName() {
        return super.getItemName();
    }

    @Override
    public String getItemDescription() {
        return super.getItemDescription();
    }


    public Integer getHealPoints() {
        return healPoints;
    }

    @Override
    public itemType getItemTypes() {
        return super.getItemTypes();
    }
}
