package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Weapons extends Inventory{
//    HOW MUCH DAMAGE WILL BE TAKEN ON THE MONSTER
    private Integer hitPoints;

    public Weapons(String itemName, String itemDescription, Integer hitPoints, itemType itemTypes) {
        super(itemName, itemDescription, itemTypes);
        this.hitPoints = hitPoints;
    }

//EMPTY CONSTRUCTOR FOR A WEAPON WITH SET STATS
    public Weapons() {

    }

    @Override
    public String getItemName() {
        return super.getItemName();
    }

    @Override
    public String getItemDescription() {
        return super.getItemDescription();
    }

    public Integer getHitPoints() {
        return hitPoints;
    }

    @Override
    public itemType getItemTypes() {
        return super.getItemTypes();
    }
}

