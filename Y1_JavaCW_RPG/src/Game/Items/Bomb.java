package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Bomb extends Weapons {

    public Bomb(String itemName, String itemDescription, Integer hitPoints, itemType itemTypes) {
        super(itemName, itemDescription,hitPoints, itemTypes);
    }

    public Bomb() {
    }

    @Override
    public String getItemName() {
        return "BOMB";
    }

    @Override
    public String getItemDescription() {
        return "An explosion will damage the enemy but the you will also be hurt";
    }

    @Override
    public Integer getHitPoints() {
        return 50;
    }

    @Override
    public itemType getItemTypes() {
        return itemType.WEAPON;
    }
}
