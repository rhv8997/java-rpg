package Game.Items;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class mushroom extends Healing {
    public mushroom(String itemName, String itemDescription, Integer healPoints, itemType itemTypes) {
        super(itemName, itemDescription, healPoints, itemTypes);

    }

    public mushroom() {
    }

    @Override
    public String getItemName() {
        return "MUSHROOM";
    }

    @Override
    public itemType getItemTypes() {
        return itemType.HEALING;
    }

    @Override
    public String getItemDescription() {
        return "A useful fungus that can clear status Conditions";
    }

    @Override
    public Integer getHealPoints() {
        return 0;
    }
}
