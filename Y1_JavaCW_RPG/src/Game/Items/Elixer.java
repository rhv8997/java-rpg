package Game.Items;

/**
 * Created by c1639984 on 28/02/2017.
 */
public class Elixer extends Healing {
    public Elixer(String itemName, String itemDescription, Integer healPoints, itemType itemTypes) {
        super(itemName, itemDescription, healPoints, itemTypes);

    }

    public Elixer() {
    }

    @Override
    public String getItemName() {
        return "ELIXER";
    }

    @Override
    public String getItemDescription() {
        return "A solutioin that will heal all status conditions and restore health to 100%";
    }

    @Override
    public Integer getHealPoints() {
        return 100;
    }

    @Override
    public itemType getItemTypes() {
        return itemType.HEALING;
    }
}
