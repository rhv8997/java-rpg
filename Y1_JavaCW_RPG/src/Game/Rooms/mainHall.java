package Game.Rooms;

import Game.Characters.Monster;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class mainHall extends Rooms {
    public mainHall(String nextRoom, String previousRoom, String leftSideOfRoom, String rightSideOfRoom, String roomDescription, String roomName, Monster centerRoom) {
        super(nextRoom, previousRoom, leftSideOfRoom, rightSideOfRoom, roomDescription, roomName, centerRoom);
    }
    @Override
    public Rooms getNextRoom() {
        return new Garden();
    }
    public mainHall() {
        super();
    }

    @Override
    public Rooms getPreviousRoom() {
        return new Dungeon();
    }

    @Override
    public void getLeftSideOfRoom() {
        System.out.println("You go and open a cupboard and find an item!");
    }

    @Override
    public void getRightSideOfRoom() {
        System.out.println("You go into the kicthen but there is a monster in there!");
    }

    @Override
    public String getRoomDescription() {
        return  "Looking to the left, there is an enterance to a cupboard \n" +
                "Looking to the right, there is an enterance to the Kitchen \n" +
                "Behind you is the Dungeon\n" +
                "In front of you is the Garden \n";
    }
    public String getRoomName() {
        return "MAIN HALL";
    }
}
