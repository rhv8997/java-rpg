package Game.Rooms;

import Game.Characters.Monster;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Dungeon extends Rooms {
    private static ArrayList<Monster> enemies = new ArrayList<Monster>();
    static Random rand = new Random();


    public Dungeon(String nextRoom, String previousRoom, String leftSideOfRoom, String rightSideOfRoom, String roomDescription, String roomName, Monster centerRoom) {
        super(nextRoom, previousRoom, leftSideOfRoom, rightSideOfRoom, roomDescription, roomName, centerRoom);
    }

    public Dungeon() {
        super();
    }

    public void getCenterRoom(Monster Enemy) {
        System.out.println("======================================================");
        System.out.println("You walk to the centre of the room then you hear a huge crash!\n" +
                "A Giant " + Enemy.getMonsterName() +" has appeared!");
    }

    @Override
    public Rooms getPreviousRoom() {
        return new mainHall();
    }


    @Override
    public String getRoomDescription() {
        return "This room is dark. There is a Treasure Chest in the centre of the room!";
    }
    public String getRoomName() {
        return "DUNGEON";
    }

}
