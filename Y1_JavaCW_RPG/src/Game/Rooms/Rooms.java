package Game.Rooms;

import Game.Characters.Monster;

import java.util.ArrayList;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Rooms {
//    ALL ROOMS WILL INHERIT FROM THIS CLASS
//    FOR OPTIONS IN ROOM, APART FROM DUNGEON WITH TWO OPTIONS
//    CHANGING ROOM WITH DISPLAY THE ROOM NAME WITH A DESCRIPTION FOR WHAT IS AROUND THE PLAYER
    private String nextRoom;
    private String previousRoom;
    private Rooms leftSideOfRoom;
    private Rooms rightSideOfRoom;
    private String roomDescription;
    public String roomName;
    private static ArrayList<Monster> enemies = new ArrayList<Monster>();



    public Rooms(String nextRoom, String previousRoom, String leftSideOfRoom, String rightSideOfRoom, String roomDescription, String roomName, Monster centerRoom) {
        this.nextRoom = nextRoom;
        this.roomName = roomName;
        this.previousRoom = previousRoom;
        this.roomDescription = roomDescription;
    }

//EMPTY CONSTRUCTOR FOR A ROOM WITH SET STATS
    public Rooms() {
    }
    public void getCenterRoom(Monster Enemy){

    }

    public Rooms getNextRoom() {
        return null;
    }

    public Rooms getPreviousRoom() {
        return null;
    }

    public void getLeftSideOfRoom() {
    }

    public void getRightSideOfRoom() {
    }

    public String getRoomDescription() {
        return null;
    }

    public String getRoomName() {
        return roomName;
    }
}
