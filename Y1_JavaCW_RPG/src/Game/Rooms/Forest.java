package Game.Rooms;

import Game.Characters.Monster;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Forest extends Rooms {
    public Forest(String nextRoom, String previousRoom, String leftSideOfRoom, String rightSideOfRoom, String roomDescription, String roomName, Monster centerRoom) {
        super(nextRoom, previousRoom, leftSideOfRoom, rightSideOfRoom, roomDescription, roomName, centerRoom);
    }

    public Forest() {
        super();
    }

    @Override
    public Rooms getNextRoom() {
        return new Lake();
    }

    @Override
    public Rooms getPreviousRoom() {
        return new Garden();
    }

    @Override
    public void getLeftSideOfRoom() {
        System.out.println("You climb up the tree and pick up an item!");
    }

    @Override
    public void getRightSideOfRoom() {
        System.out.println("You go to the leaves but then something jumps at you!");
    }

    @Override
    public String getRoomDescription() {
        return "Looking to the left, there is a tree you can climb up\n" +
                "Looking to the right, there is a pile of fallen leaves you can look through\n" +
                "Behind you is the Garden\n" +
                "Infront of you is the Lake\n";
    }
    public String getRoomName() {
        return "FOREST";
    }
}

