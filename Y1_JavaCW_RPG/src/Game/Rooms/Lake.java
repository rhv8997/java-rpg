package Game.Rooms;

import Game.Characters.Monster;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Lake extends Rooms {
    public Lake(String nextRoom, String previousRoom, String leftSideOfRoom, String rightSideOfRoom, String roomDescription, String roomName, Monster centerRoom) {
        super(nextRoom, previousRoom, leftSideOfRoom, rightSideOfRoom, roomDescription, roomName, centerRoom);
    }
    @Override
    public Rooms getNextRoom() {
        return new mainHall();
    }


    public Lake() {
        super();
    }

    @Override
    public Rooms getPreviousRoom() {
        return new Forest();
    }

    @Override
    public void getLeftSideOfRoom() {
        System.out.println("You go other to the boat and find an item hidden within it!");
    }

    @Override
    public void getRightSideOfRoom() {
        System.out.println("You go other to the enterance of the lake and a monster appears!");
    }

    @Override
    public String getRoomDescription() {
        return  "Looking to the left, a boat docked\n" +
                "Looking to the right, there is an enterance to the lake \n" +
                "Behind you is the Forest\n" +
                "In front of you is the an entrance back to the main hall";
    }
    public String getRoomName() {
        return "LAKE";
    }
}

