package Game.Rooms;

import Game.Characters.Monster;

/**
 * Created by c1639984 on 21/02/2017.
 */
public class Garden extends Rooms {

    public Garden(String nextRoom, String previousRoom, String leftSideOfRoom, String rightSideOfRoom, String roomDescription, String roomName, Monster centerRoom) {
        super(nextRoom, previousRoom, leftSideOfRoom, rightSideOfRoom, roomDescription, roomName, centerRoom);
    }
    public Garden() {
        super();
    }
    @Override
    public Rooms getNextRoom() {
        return new Forest();
    }

    @Override
    public Rooms getPreviousRoom() {
        return new mainHall();
    }

    @Override
    public void getLeftSideOfRoom() {
        System.out.println("You go over to the table to find an item on top of it!");
    }

    @Override
    public void getRightSideOfRoom() {
        System.out.println("You go over and look in the shed but something jumps out!!");
    }

    @Override
    public String getRoomDescription() {
        return  "Looking to the left, there is a garden table\n" +
                "Looking to the right, there is an open shed \n" +
                "Behind you is the Main Hall\n" +
                "In front of you is the Foresr \n";
    }
    public String getRoomName() {
        return "GARDEN";
    }
}

