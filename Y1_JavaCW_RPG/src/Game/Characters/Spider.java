package Game.Characters;

/**
 * Created by c1639984 on 28/02/2017.
 */
public class Spider extends Monster {
    private Integer monsterHealth = 50;

    public Spider(String monsterName, String monsterAttack, Integer monsterHealth, Integer maxMonsterAttack, statusCondition inflictStatusCondition) {
        super(monsterName, monsterAttack, monsterHealth, maxMonsterAttack, inflictStatusCondition);
    }
    public Spider() {
    }
    @Override
    public String getMonsterName() {
        return "SPIDER";
    }

    @Override
    public String getMonsterAttack() {
        return "BITES";
    }

    @Override
    public Integer getMonsterHealth() {
        return monsterHealth;
    }

    @Override
    public Integer getMaxMonsterAttack() {
        return 40;
    }

    @Override
    public statusCondition getInflictStatusCondition() {
        return statusCondition.POISONED;
    }
    @Override
    public void setMonsterName(String monsterName) {
        super.setMonsterName(monsterName);
    }

    @Override
    public void setMonsterHealth(Integer monsterHealth) {
        this.monsterHealth = monsterHealth;
    }
}
