package Game.Characters;

/**
 * Created by c1639984 on 27/02/2017.
 */
//THE DIFFERENT STATUS CONDITONS THAT COULD BE INFLICTED ON THE USER
public enum statusCondition {
    FROZEN, BURNED, POISONED, DIZZY, NONE;
}
