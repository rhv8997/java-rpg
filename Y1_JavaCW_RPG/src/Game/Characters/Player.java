package Game.Characters;

import Game.Characters.statusCondition;


/**
 * Created by c1639984 on 21/02/2017.
 */
//A PLAYER WITH SET STATISTICS THAT CAN BE CHANGED WITHIN THE GAME
public class Player {
//    IF THIS IS AFFECTED, THIS CAN ONLY BE HEALED BY REMOVING THE STATUS CONDTION AFFECTING IT.
//    NOT BEING ABLE TO RESTORE THIS MEANS THE PLAYER MUST COMPLETE THE GAME IN THE FEWEST ROOM CHANGES POSSIBLE
    private Integer maxEnergyLevel = 50;
    private statusCondition CurrentStatusCondition = statusCondition.NONE;
    private Integer maxHealthPoints = 100;


    public Player(Integer maxEnergyLevel, statusCondition currentStatusCondition, Integer maxHealthPoints) {
        this.maxEnergyLevel = maxEnergyLevel;
        CurrentStatusCondition = currentStatusCondition;
        this.maxHealthPoints = maxHealthPoints;
    }
//EMPTY CONSTRUCTOR FOR A PLAYER WITH SET STATS

    public Player() {
    }

    public Integer getMaxEnergyLevel() {
        return maxEnergyLevel;
    }

    public void setMaxEnergyLevel(Integer maxEnergyLevel) {
        this.maxEnergyLevel = maxEnergyLevel;
    }

    public statusCondition getCurrentStatusCondition() {
        return CurrentStatusCondition;
    }

    public void setCurrentStatusCondition(statusCondition currentStatusCondition) {
        CurrentStatusCondition = currentStatusCondition;
    }

    public Integer getMaxHealthPoints() {
        return maxHealthPoints;
    }

    public void setMaxHealthPoints(Integer maxHealthPoints) {
        this.maxHealthPoints = maxHealthPoints;
    }
}