package Game.Characters;

/**
 * Created by c1639984 on 28/02/2017.
 */
public class Siren extends Monster {
    private Integer monsterHealth = 75;

    public Siren(String monsterName, String monsterAttack, Integer monsterHealth, Integer maxMonsterAttack, statusCondition inflictStatusCondition) {
        super(monsterName, monsterAttack, monsterHealth, maxMonsterAttack, inflictStatusCondition);

    }

    public Siren() {
    }
    @Override
    public void setMonsterName(String monsterName) {
        super.setMonsterName(monsterName);
    }
    @Override
    public String getMonsterName() {
        return "SIREN";
    }

    @Override
    public String getMonsterAttack() {
        return "HYPNOTISED";
    }

    @Override
    public Integer getMonsterHealth() {
        return monsterHealth;
    }

    @Override
    public Integer getMaxMonsterAttack() {
        return 35;
    }

    @Override
    public statusCondition getInflictStatusCondition() {
        return statusCondition.FROZEN;
    }

    @Override
    public void setMonsterHealth(Integer monsterHealth) {
        this.monsterHealth = monsterHealth;
    }
}
