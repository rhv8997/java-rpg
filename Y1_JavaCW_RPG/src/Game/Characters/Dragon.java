package Game.Characters;

/**
 * Created by c1639984 on 28/02/2017.
 */
public class Dragon extends Monster {

    private Integer monsterHealth = 100;

    public Dragon(String monsterName, String monsterAttack, Integer monsterHealth, Integer maxMonsterAttack, statusCondition inflictStatusCondition) {
        super(monsterName, monsterAttack, monsterHealth, maxMonsterAttack, inflictStatusCondition);

    }

    public Dragon() {
    }

    @Override
    public String getMonsterName() {
        return "DRAGON";
    }

    @Override
    public String getMonsterAttack() {
        return "BREATHED FIRE AT";
    }

    @Override
    public Integer getMonsterHealth() {
        return monsterHealth;
    }

    @Override
    public Integer getMaxMonsterAttack() {
        return 50;
    }

    @Override
    public statusCondition getInflictStatusCondition() {
        return statusCondition.BURNED;
    }

    @Override
    public void setMonsterHealth(Integer monsterHealth) {this.monsterHealth = monsterHealth;    }

    @Override
    public void setMonsterName(String monsterName) {
        super.setMonsterName(monsterName);
    }
}
