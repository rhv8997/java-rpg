package Game.Characters;

/**
 * Created by c1639984 on 28/02/2017.
 */
public class Gnome extends Monster {
    private Integer monsterHealth=60;

    public Gnome(String monsterName, String monsterAttack, Integer monsterHealth, Integer maxMonsterAttack, statusCondition inflictStatusCondition) {
        super(monsterName, monsterAttack, monsterHealth, maxMonsterAttack, inflictStatusCondition);

    }

    public Gnome() {
    }
    @Override
    public String getMonsterName() {
        return "GNOME";
    }
    @Override
    public void setMonsterName(String monsterName) {
        super.setMonsterName(monsterName);
    }

    @Override
    public String getMonsterAttack() {
        return "STABBED";
    }

    @Override
    public Integer getMonsterHealth() {
        return monsterHealth;
    }

    @Override
    public Integer getMaxMonsterAttack() {
        return 20;
    }

    @Override
    public statusCondition getInflictStatusCondition() {
        return statusCondition.NONE;
    }

    @Override
    public void setMonsterHealth(Integer monsterHealth) {
        this.monsterHealth = monsterHealth;
    }
}
