package Game.Characters;

/**
 * Created by c1639984 on 21/02/2017.
 */
//A CLASS ALL VARIATIONS OF MONSTERS WILL INHEREIT FROM
public class Monster{
    private String monsterName;
    private String monsterAttack;
    private Integer monsterHealth;
//    A RANDOM NUMBER BETWEEN 0 AND THE MAX MONSTER ATTACK WILL BE USED
    private Integer maxMonsterAttack;
//    A STATUS WILL BE INFLICTED UPON THE USER TO MAKE THE GAME HARDER
    private statusCondition inflictStatusCondition;

    public Monster(String monsterName, String monsterAttack, Integer monsterHealth, Integer maxMonsterAttack,statusCondition inflictStatusCondition) {
        this.monsterName = monsterName;
        this.monsterAttack = monsterAttack;
        this.monsterName = monsterName;
        this.maxMonsterAttack = maxMonsterAttack;
        this.inflictStatusCondition = inflictStatusCondition;
    }
    public Monster() {
    }
    public String getMonsterName() {
        return monsterName;
    }
    public String getMonsterAttack() {
        return monsterAttack;
    }
    public Integer getMonsterHealth() {
        return monsterHealth;
    }
    public Integer getMaxMonsterAttack() {
        return maxMonsterAttack;
    }
    public statusCondition getInflictStatusCondition() {
        return inflictStatusCondition;
    }
    public void setMonsterHealth(Integer monsterHealth) {
        this.monsterHealth = monsterHealth;
    }
    public void setMonsterName(String monsterName) {
        this.monsterName = monsterName;
    }
}