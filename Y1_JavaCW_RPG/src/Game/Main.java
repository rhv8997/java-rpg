package Game;
import Game.Characters.*;
import Game.Items.Inventory;
import Game.Items.*;
import Game.Rooms.*;

import java.util.*;
/**
 * Created by c1639984 on 27/02/2017.
 */
public class Main {
    private static Scanner inputScanner = new Scanner(System.in);
    private static Random rand = new Random();
    private Boolean inPlay = false;
//    CREATE THE LISTS FOR THE ITEMS, MONSTERS AND ROOMS THAT'LL BE USED
    private static ArrayList<Rooms> RoomsAvailable = new ArrayList<Rooms>();
    private static ArrayList<Monster> enemies = new ArrayList<Monster>();
    private static ArrayList<Weapons> weaponInventory = new ArrayList<Weapons>();
    private static ArrayList<Healing> healingInventory = new ArrayList<Healing>();
    private static ArrayList<Inventory> findableItems = new ArrayList<Inventory>();
//    THE PLAYER IS CREATED
    private static Player currentPlayer = new Player();




//    DISPLAY HEALTH INFORMATION ABOUT THE PLAYER
    public static void playerHealthDetails(){
        System.out.println("======================================================");
        System.out.println("\tSTATUS CONDITION: " + getCurrentPlayer().getCurrentStatusCondition());
        System.out.println("\tENEGRY LEVEL: " + getCurrentPlayer().getMaxEnergyLevel());
        System.out.println("\tHEALTH POINTS: " + getCurrentPlayer().getMaxHealthPoints());
        System.out.println("======================================================");
    }

//    ADD ALL OF THE OBJECTS TO THE CORRECT CLASSES BEFORE THE GAME BEGINS
    public static void addItemsToInventory(){
        getEnemies().add(new Dragon());
        getEnemies().add(new Gnome());
        getEnemies().add(new Siren());
        getEnemies().add(new Spider());
        getRoomsAvailable().add(new Dungeon());
        getRoomsAvailable().add(new Forest());
        getRoomsAvailable().add(new Garden());
        getRoomsAvailable().add(new Lake());
        getRoomsAvailable().add(new mainHall());
        getWeaponInventory().add(new Sword());
        getWeaponInventory().add(new Wand());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new Potion());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
        getHealingInventory().add(new mushroom());
        getFindableItems().add (new goldenKey());
        getFindableItems().add(new mushroom());
        getFindableItems().add(new Bomb());
        getFindableItems().add(new Potion());
        getFindableItems().add (new Elixer());
    }

//       SHOW WHAT YOU'VE ENCOUNTERED AND HEALTH STATS
    public static void monsterFirstEncounter(Monster enemy){
        System.out.println("======================================================");
        System.out.println("\t YOU HAVE ENCOUNTERED A " + enemy.getMonsterName()+ "!");
        System.out.println("YOUR HEALTH: " + getCurrentPlayer().getMaxHealthPoints());
        System.out.println("ENEMY HEALTH: " + enemy.getMonsterHealth());
    }

//    PLAYER OPTION MENU TO RUN, HEAL OR ATTACK
    public static void runHealAttackOption(){
        System.out.println("======================================================");
        System.out.println("YOUR MOVE:");
        System.out.println("\t [1]-ATTACK");
        System.out.println("\t [2]-HEAL");
        System.out.println("\t [3]-RUN");
        System.out.print("--->");
    }

//   WHERE DO YOU WANT TO GO IN THE ROOM MENU
    public static void currentRoomOption(){
        System.out.println("======================================================");
        System.out.println("WHERE DO YOU WANT TO GO?");
        System.out.println("YOUR MOVE:");
        System.out.println("\t [1]-LEFT");
        System.out.println("\t [2]-RIGHT");
        System.out.println("\t [3]-FORWARD");
        System.out.println("\t [4]-BEHIND");
        System.out.print("--->");
    }

//   DIFFERENT MENU IF YOU'RE IN THE DUNGEON
    public static void DungeonRoomOption(){
        System.out.println("======================================================");
        System.out.println("WHERE DO YOU WANT TO GO?");
        System.out.println("YOUR MOVE:");
        System.out.println("\t [1]-GO FORWARD");
        System.out.println("\t [2]-LEAVE");
        System.out.print("--->");
    }

//   DISPLAY ALL WEAPONS IN INVENTORY (APART FROM A GOLDEN KEY)
    public static void displayWeapons(){
        System.out.println("======================================================");
        System.out.print("YOUR WEAPONS: ");
        for (int i = 1; i < getWeaponInventory().size()+1; i++) {
            if (!(getWeaponInventory().get(i-1).getItemName().equals("GOLDEN KEY"))){
            System.out.print("|" + i +"|");
            System.out.print("|" + (getWeaponInventory().get(i-1)).getItemName());
        }

    }     System.out.println("|");
        System.out.println("======================================================");
    }

//   DISPLAY ALL HEALING ITEMS IN INVENTORY
    public static void displayHealingItems(){
        System.out.println("======================================================");
        System.out.print("YOUR HEALING ITEMS: ");
        for (int i = 1; i < getHealingInventory().size()+1; i++) {
            System.out.print("|"+ i + "|");

            System.out.print("|" + (getHealingInventory().get(i-1)).getItemName());
        }
        System.out.println("|");
        System.out.println("======================================================");
    }

//   OPTIONS FOR SELECTED WEAPON (WEAPON SELECTED EITHER BY NAME OR INDEX)
    public static void attackOption(String playerInputWeapon, Monster enemy){

        boolean inInvetory = false;
        while  (!(inInvetory)){
            if (playerInputWeapon.matches(".*\\d.*"));
                int weaponIndex = Integer.parseInt(playerInputWeapon);
                if (weaponIndex >= 0 | weaponIndex< getWeaponInventory().size()){
                    Weapons currentWeapon = getWeaponInventory().get(weaponIndex-1);
                    System.out.println("======================================================");
                    System.out.println("YOU HAVE SELECTED A " + currentWeapon.getItemName());
                    System.out.println("WHAT DO YOU WANT TO DO WITH THE " + currentWeapon.getItemName() + "?");
                    System.out.println("\t [1]-ATTACK");
                    System.out.println("\t [2]-READ DESCRIPTION");
                    System.out.print("--->");
                    Integer playerInputWeaponOption = getInputScanner().nextInt();
                    System.out.println("======================================================");
                    weaponOption(enemy, currentWeapon, playerInputWeaponOption);
                    inInvetory = true;
                }
                else{
                    for (int i = 0; i< getWeaponInventory().size(); i++){
                        Weapons currentWeapon = getWeaponInventory().get(i);
                        if (currentWeapon.getItemName().equals(playerInputWeapon.toUpperCase())) {
                            System.out.println("YOU HAVE SELECTED A " + currentWeapon.getItemName());
                            System.out.println("WHAT DO YOU WANT TO DO WITH THE " + currentWeapon.getItemName() + "?");
                            System.out.println("\t [1]-ATTACK");
                            System.out.println("\t [2]-READ DESCRIPTION");
                            System.out.print("--->");
                            Integer playerInputWeaponOption = getInputScanner().nextInt();
                            System.out.println("======================================================");
                            weaponOption(enemy, currentWeapon, playerInputWeaponOption);
                            inInvetory = true;

                        }}
                }
        }}

//   OPTIONS FOR SELECTED HEALING ITEMS (HEALING ITEMS SELECTED EITHER BY NAME OR INDEX)
    public static void healingOption(String playerInputHeal, Monster enemy){

        boolean inInvetory = false;
        while  (!(inInvetory)){
            if (playerInputHeal.matches(".*\\d.*"));
            int healIndex = Integer.parseInt(playerInputHeal);
            if (healIndex >= 0 | healIndex< getHealingInventory().size()){
                Healing currentHealingItem = getHealingInventory().get(healIndex-1);
                System.out.println("YOU HAVE SELECTED A " + currentHealingItem.getItemName());
                System.out.println("WHAT DO YOU WANT TO DO WITH THE " + currentHealingItem.getItemName() + "?");
                System.out.println("\t [1]-USE");
                System.out.println("\t [2]-READ DESCRIPTION");
                System.out.print("--->");
                Integer playerInputHealingOption = getInputScanner().nextInt();
                System.out.println("======================================================");
                healOption(enemy, currentHealingItem, playerInputHealingOption);
                inInvetory = true;
            }
            else{
            for (int i = 0; i< getHealingInventory().size(); i++){
                Healing currentHealingItem = getHealingInventory().get(i);
                if (currentHealingItem.getItemName().equals(playerInputHeal.toUpperCase())) {
                    System.out.println("YOU HAVE SELECTED A " + currentHealingItem.getItemName());
                    System.out.println("WHAT DO YOU WANT TO DO WITH THE " + currentHealingItem.getItemName() + "?");
                    System.out.println("\t [1]-USE");
                    System.out.println("\t [2]-READ DESCRIPTION");
                    System.out.print("--->");
                    Integer playerInputHealingOption = getInputScanner().nextInt();
                    System.out.println("======================================================");
                    healOption(enemy,currentHealingItem, playerInputHealingOption);
                    inInvetory = true;

                }}
        }}}

//   IF PLAYER HAS A STATUS CONDITION, THIS WILL EFFECT HOW THEY CAN PLAY
    public static void statusConditionConsqeuence(Monster enemy, Weapons currentWeapon){
        if (getCurrentPlayer().getCurrentStatusCondition().equals(statusCondition.BURNED)){
            System.out.println("======================================================");
            System.out.println("YOU HP WILL DECREASE BY 10 POINT EACH ROUND");
            System.out.println("======================================================");

            getCurrentPlayer().setMaxHealthPoints(getCurrentPlayer().getMaxHealthPoints()-10);
            playerAttacksMonster(enemy, currentWeapon);
        }else if (getCurrentPlayer().getCurrentStatusCondition().equals(statusCondition.DIZZY)){
            Integer dizzyHitMonster = getRand().nextInt(2);
            if (dizzyHitMonster == 1){
                playerAttacksMonster(enemy, currentWeapon);
            }else
                System.out.println("======================================================");
                System.out.println("YOU ARE DIZZY AND MISSED THE " + enemy.getMonsterName());
                System.out.println("======================================================");

        }else if (getCurrentPlayer().getCurrentStatusCondition().equals(statusCondition.FROZEN)){
            System.out.println("======================================================");
            System.out.println("YOU ARE FROZEN AND CANNOT ATTACK");
        }else if (getCurrentPlayer().getCurrentStatusCondition().equals(statusCondition.POISONED)){
            System.out.println("======================================================");
            System.out.println("YOU ENERGY WILL DECREASE BY 10 POINT EACH ROUND");
            System.out.println("======================================================");

            getCurrentPlayer().setMaxEnergyLevel(getCurrentPlayer().getMaxEnergyLevel()-10);
            playerAttacksMonster(enemy, currentWeapon);
        }else if (getCurrentPlayer().getCurrentStatusCondition().equals(statusCondition.NONE)){
            playerAttacksMonster(enemy,currentWeapon);
        }
    }

//   DECREASE MONSTER HEALTH
    public static void playerAttacksMonster(Monster enemy, Weapons currentWeapon){
        System.out.println("YOU ATTACKED THE " + enemy.getMonsterName());
        enemy.setMonsterHealth(enemy.getMonsterHealth()-currentWeapon.getHitPoints());
//        BOMB CAN'T BE REUSED
        if (currentWeapon.getItemName().toString().equals("BOMB")){
            getWeaponInventory().remove(currentWeapon);
        }
    }

//    FIND ITEM AND PUT IT IN THE CORRECT INVENTORY
    public static void collectItem(){
        Inventory itemFound = getFindableItems().get(getRand().nextInt(getFindableItems().size()));
        System.out.println("\n======================================================");
        System.out.println("YOU HAVE FOUND A " + itemFound.getItemName());
        System.out.println("YOU PUT THE " + itemFound.getItemName() + " IN YOU INVENTORY");
        if (itemFound.getItemTypes().equals(itemType.HEALING)){
            Healing healingItemFound = (Healing) itemFound;
            getHealingInventory().add(healingItemFound);
        }else if (itemFound.getItemTypes().equals(itemType.WEAPON)){
            Weapons weaponFound = (Weapons) itemFound;
            getWeaponInventory().add(weaponFound);
        }
        System.out.println("======================================================");

    }

//   MONSTER ATTACKS YOU AND YOUR STATS ARE AFFECTED. YOU CAN KILL THE MONSTER OR YOU CAN BE KILLED
    public static void attackMonster(Monster enemy, Weapons currentWeapon){
        if (enemy.getMonsterHealth()>0){
            System.out.println("THE " + enemy.getMonsterName() + " " + enemy.getMonsterAttack() + " YOU!");
            Integer enemyAttackDamage = getRand().nextInt(enemy.getMaxMonsterAttack());
            getCurrentPlayer().setMaxHealthPoints(getCurrentPlayer().getMaxHealthPoints()-enemyAttackDamage);
            Integer inflictedConditionPercentage = getRand().nextInt(2);
//            RANDOM CHANCE YOU'LL ET A STATUS CONDITION
            if (inflictedConditionPercentage == 1){
                if(!(getCurrentPlayer().getCurrentStatusCondition().toString().equals(enemy.getInflictStatusCondition().toString()))) {
                    getCurrentPlayer().setCurrentStatusCondition(enemy.getInflictStatusCondition());
                    System.out.println("THE " + enemy.getMonsterName() + " HAS " + enemy.getInflictStatusCondition().toString() + " YOU!");
                    playerHealthDetails();
                }
            }
            statusConditionConsqeuence(enemy, currentWeapon);
            System.out.println("======================================================");
            System.out.println("ENEMY HEALTH: " + enemy.getMonsterHealth());
            System.out.println("PLAYER HEALTH: " + getCurrentPlayer().getMaxHealthPoints());
//            System.out.println("======================================================");
            if (enemy.getMonsterHealth()<=0) {
                System.out.println("======================================================");
                System.out.print("YOU DEFEATED THE " + enemy.getMonsterName());
                collectItem();
            }else if (getCurrentPlayer().getMaxHealthPoints()<=0 | getCurrentPlayer().getMaxEnergyLevel()<=0){
                System.out.println("======================================================");
                System.out.print("THE " + enemy.getMonsterName() +" HAS DEFEATED YOU");
                System.out.println("\n======================================================");

            }else{
                runHealAttackOption();
            }
        }else if (enemy.getMonsterHealth() <=0){
            System.out.println("======================================================");
            System.out.print("YOU DEFEATED THE " + enemy.getMonsterName());
            System.out.println("======================================================");

            collectItem();
        }else{
            System.out.print("THERE IS AN ERROR SOMEWHERE");
    }}

//    USE A POTION TO RESTORE PLAYER HEALTH POINTS TO 100 AND STATE HOW MUCH POTION WAS USED
    public static void usedPotion(Healing currentHealingItem){
        Integer amountPotionUsed = getCurrentPlayer().getMaxHealthPoints()+currentHealingItem.getHealPoints();
        if (amountPotionUsed >= 100){
            Integer leftOverPotionAmount = currentHealingItem.getHealPoints() - getCurrentPlayer().getMaxHealthPoints();
            System.out.println(leftOverPotionAmount + "hp OF THE POTION USED");
            getCurrentPlayer().setMaxHealthPoints(100);
        }else{
            System.out.println("100hp OF THE POTION USED");
            getCurrentPlayer().setMaxHealthPoints(100);
        }
    }

//    USE A HEALING ITEM AND IT WILL DO WHAT IT NEEDS TO DO
    public static void healSelf(Monster enemy, Healing currentHealingItem){
        System.out.println("YOU USED THE " + currentHealingItem.getItemName());
        if (currentHealingItem.getItemName().toString().equals("MUSHROOM")){
            System.out.println("ALL STATUS CONDITIONS HAVE BEEN HEALED");
            getCurrentPlayer().setCurrentStatusCondition(statusCondition.NONE);
            playerHealthDetails();
            runHealAttackOption();
            getHealingInventory().remove(currentHealingItem);
        }else if (currentHealingItem.getItemName().toString().equals("POTION")){
            System.out.println("HEALTH HAS BEEN RESTORED TO 100%");
            usedPotion(currentHealingItem);
            playerHealthDetails();
            runHealAttackOption();
            getHealingInventory().remove(currentHealingItem);

        }else if (currentHealingItem.getItemName().toString().equals("ELIXER")){
            System.out.println("HEALTH HAS BEEN RESTORED TO 100%");
            System.out.println("ALL STATUS CONDITIONS HAVE BEEN HEALED");
            usedPotion(currentHealingItem);
            playerHealthDetails();
            runHealAttackOption();
            getHealingInventory().remove(currentHealingItem);

        }}

//        CHOOSE WHAT TO DO WITH THE WEAPON SELECTED
    public static void weaponOption(Monster enemy, Weapons currentWeapon, Integer playerInputWeaponOption){
        if (playerInputWeaponOption == 1) {
            attackMonster(enemy, currentWeapon);
        }else if (playerInputWeaponOption == 2) {
            System.out.println("THIS IS THE DESCRIPTION OF THE " + currentWeapon.getItemName());
            System.out.println(currentWeapon.getItemDescription());
            System.out.println("DO YOU STILL WANT TO USE THE " + currentWeapon.getItemName() + "? [Y/N}");
            System.out.print("--->");
            String playerInputWeapon = getInputScanner().next();
            if (playerInputWeapon.toUpperCase().equals("Y")) {
                attackMonster( enemy, currentWeapon);
            } else if (playerInputWeapon.toUpperCase().equals("N")) {
                runHealAttackOption();
            }
        }

        }

//        CHOOSE WHAT TO DO WITH THE HEALING ITEM SELECTED
    public static void healOption(Monster enemy, Healing currentHealingItem, Integer playerInputWeaponOption){
        if (playerInputWeaponOption == 1) {
            healSelf(enemy, currentHealingItem);
        }else if (playerInputWeaponOption == 2) {
            System.out.println("THIS IS THE DESCRIPTION OF THE " + currentHealingItem.getItemName());
            System.out.println(currentHealingItem.getItemDescription());
            System.out.println("DO YOU STILL WANT TO USE THE " + currentHealingItem.getItemName() + "? [Y/N}");
            System.out.print("--->");
            String playerInputWeapon = getInputScanner().next();
            if (playerInputWeapon.toUpperCase().equals("Y")) {
                healSelf( enemy, currentHealingItem);
            } else if (playerInputWeapon.toUpperCase().equals("N")) {
                runHealAttackOption();
            }
        }

    }

//    SIMPLE WELCOME MESSAGE TO START THE GAME
    public static void welcomeMessage(){
        System.out.println("======================================================");
        System.out.println("\t\t\t\tWELCOME TO THE GAME");
    }

//    DO YOU WANT TO ATTACK THE MONSTER, HEAL OR RUN AWAY
    public static void enemyEncounter(Monster Enemy, Rooms currentRoom){
        String roomname = currentRoom.getRoomName().toString();

        while (Enemy.getMonsterHealth() > 0) {
            Integer playerInputMove = getInputScanner().nextInt();
            if (playerInputMove == 1) {
                displayWeapons();
                System.out.println("WHAT WEAPON DO YOU WANT TO USE?");
                System.out.print("--->");
                String playerInputWeapon = getInputScanner().next();
                attackOption(playerInputWeapon, Enemy);
            }else if (playerInputMove == 2){
                displayHealingItems();
                System.out.println("WHAT HEALING ITEM DO YOU WANT TO USE?");
                System.out.print("--->");
                String playerInputHealing = getInputScanner().next();
                healingOption(playerInputHealing, Enemy);
            }else if (playerInputMove == 3){
                System.out.println("YOU RAN FROM THE " + Enemy.getMonsterName());
                System.out.println("======================================================");
                playTheGame(currentRoom);
            }else{
                System.out.println("======================================================");
                System.out.println("THAT IS NOT A VALID INPUT.");
                runHealAttackOption();

            }

            }
        if (roomname.equals("DUNGEON")) {
            System.out.println("YOU SEE THE TREASURE CHEST");
            for (int i = 0; i < getWeaponInventory().size(); i++) {
                if ((getWeaponInventory().get(i).getItemName().equals("GOLDEN KEY"))) {
                    System.out.println("YOU OPEN THE CHEST AND REALISE NOTHING IS IN IT AND YOU'VE WASTED YOU TIME \n THE END!");
                    System.exit(1);
                }
            }
            System.out.println("YOU DONT HAVE THE GOLDEN KEY! GO AND LOOK FOR IT");



        }}

//    WHAT ROOM ARE YOU IN. COLLECT ITEM TO LEFT, ENEMY TO RIGHT, GO FORWARD OR BACK. DIFFERENT FOR DUNGEON
//    EACH TIME YOU MOVE ROOMS, YOU ENERGY DECREASES
    public static void playTheGame(Rooms currentRoom){
            Boolean inPlay = true;
            while (inPlay) {
                playerHealthDetails();
                System.out.println("YOU ARE IN THE " + currentRoom.getRoomName());
                System.out.println("======================================================");
                System.out.println(currentRoom.getRoomDescription());

                String roomname = currentRoom.getRoomName().toString();
                if (!(roomname.equals("DUNGEON"))) {
                    currentRoomOption();
                    String roomOptions = getInputScanner().next();
                    if (roomOptions.matches(".*\\d.*")) {
                        if (roomOptions.equals("1")) {
                            currentRoom.getLeftSideOfRoom();
                            collectItem();
                        } else if (roomOptions.equals("2")) {
                            Monster Enemy = getEnemies().get(getRand().nextInt(getEnemies().size()));
                            monsterFirstEncounter(Enemy);
                            runHealAttackOption();
                            enemyEncounter(Enemy, currentRoom);
                        } else if (roomOptions.equals("3")) {
                            getCurrentPlayer().setMaxEnergyLevel(getCurrentPlayer().getMaxEnergyLevel()-5);
                            if (getCurrentPlayer().getMaxEnergyLevel()<0){
                                System.out.print("YOU HAVE RAN OUT OF ENERGY AND YOU HAVE FAINTED");
                                System.exit(1);

                            }
                            currentRoom = currentRoom.getNextRoom();
                        } else if (roomOptions.equals("4")) {
                            getCurrentPlayer().setMaxEnergyLevel(getCurrentPlayer().getMaxEnergyLevel()-5);
                            if (getCurrentPlayer().getMaxEnergyLevel()<0){
                                System.out.print("YOU HAVE RAN OUT OF ENERGY AND YOU HAVE FAINTED");
                                System.exit(1);

                            }

                            currentRoom = currentRoom.getPreviousRoom();
                        } else {
                            System.out.print("ERROR!");
                        }
                    }
                } else if (roomname.equals("DUNGEON")) {

                    DungeonRoomOption();
                    String roomOptions = getInputScanner().next();
                    if (roomOptions.matches(".*\\d.*")) {
                        if (roomOptions.equals("1")) {
                            Monster Enemy = getEnemies().get(getRand().nextInt(getEnemies().size()));
                            Enemy.setMonsterHealth(150);
                            currentRoom.getCenterRoom(Enemy
                            );
                            monsterFirstEncounter(Enemy);
                            runHealAttackOption();
                            enemyEncounter(Enemy, currentRoom);


                        }else if (roomOptions.equals("2")){
                            getCurrentPlayer().setMaxEnergyLevel(getCurrentPlayer().getMaxEnergyLevel()-5);
                            if (getCurrentPlayer().getMaxEnergyLevel()<0){
                                System.out.print("YOU HAVE RAN OUT OF ENERGY AND YOU HAVE FAINTED");
                                System.exit(1);

                            }
                            currentRoom = currentRoom.getPreviousRoom();

                        } else {
                            System.out.print("ERROR!");
                        }
                    }


                }
            }
        }

//     MAIN METHOD TO CALL OTHER METHODS TO PLAY THE GAME
    public static void main(String[] args) {
        welcomeMessage();
        Rooms currentRoom = new mainHall();
        addItemsToInventory();
        playTheGame(currentRoom);

    }

    public static Scanner getInputScanner() {
        return inputScanner;
    }

    public static void setInputScanner(Scanner inputScanner) {
        Main.inputScanner = inputScanner;
    }

    public static Random getRand() {
        return rand;
    }

    public static void setRand(Random rand) {
        Main.rand = rand;
    }

    public static ArrayList<Rooms> getRoomsAvailable() {
        return RoomsAvailable;
    }

    public static void setRoomsAvailable(ArrayList<Rooms> roomsAvailable) {
        RoomsAvailable = roomsAvailable;
    }

    public static ArrayList<Monster> getEnemies() {
        return enemies;
    }

    public static void setEnemies(ArrayList<Monster> enemies) {
        Main.enemies = enemies;
    }

    public static ArrayList<Weapons> getWeaponInventory() {
        return weaponInventory;
    }

    public static void setWeaponInventory(ArrayList<Weapons> weaponInventory) {
        Main.weaponInventory = weaponInventory;
    }

    public static ArrayList<Healing> getHealingInventory() {
        return healingInventory;
    }

    public static void setHealingInventory(ArrayList<Healing> healingInventory) {
        Main.healingInventory = healingInventory;
    }

    public static ArrayList<Inventory> getFindableItems() {
        return findableItems;
    }

    public static void setFindableItems(ArrayList<Inventory> findableItems) {
        Main.findableItems = findableItems;
    }

    public static Player getCurrentPlayer() {
        return currentPlayer;
    }

    public static void setCurrentPlayer(Player currentPlayer) {
        Main.currentPlayer = currentPlayer;
    }

    public Boolean getInPlay() {
        return inPlay;
    }

    public void setInPlay(Boolean inPlay) {
        this.inPlay = inPlay;
    }
}

